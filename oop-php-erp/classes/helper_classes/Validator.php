<?php
class Validator
{
    private $di;
    protected $database;
    protected $errorHandler;
    
    protected $rules = ["required", 'minlength', "maxlength", "unique", "email"];
    
    protected $messages = [
        "required" => "This :field field is required",
        "minlength" => "This :field must be a minimum of :satisfier characters",
        "maxlength" => "This :field must be a maximum of :satisfier characters",
        "email" => "This is not a valid email address",
        "unique" => "That :field is laready taken"
    ];
    
    public function __construct(DependencyInjector $di)
    {
        $this->di = $di;
        $this->database = $this->di->get('database');
        $this->errorHandler = $this->di->get('error_handler');
    }
    public function check($items, $rules)
    {
        foreach($items as $item=>$value)
        {
            if(in_array($item, array_keys($rules)))
            {
                $this->validate([
                   'field'=>$item,
                    'value'=>$value,
                    'rules'=>$rules[$item]
                ]);
            }
        }
        return $this;
    }
    public function fails()
    {
        return $this->errorHandler->hasErrors();
    }
    public function errors()
    {
        return $this->errorHandler;
    }
    
    private function validate($item)
    {
        //value of that rule is know as satisfier
        
        $field = $item['field'];
        $value = $item['value'];
        foreach($item['rules'] as $rule=>$satisfier)
        {
            if(!call_user_func_array([$this, $rule], [$field, $value, $satisfier]))
            {
                $this->errorHandler->addError(str_replace([':field', ':satisfier'], [$field, $satisfier], $this->messages[$rule]), $field);
            }
                                    
        }
    }
    private function required($field, $value, $satisfier)
    {
        return !empty(trim($value));
    }
    private function minlength($field, $value, $satisfier)
    {
        return mb_strlen($value)>=$satisfier;
    }
    private function maxlength($field, $value, $satisfier)
    {
        return mb_strlen($value)<=$satisfier;
    }
    private function unique($field, $value, $satisfier)
    {
        //Here $satisfier will become the name of table!
        //$field will become the name of column
        //$value should be unique under the above table and column 
        return !$this->database->exists($satisfier, [$field=>$value]);
    }
    private function email($field, $value, $satisfier)
    {
        return filter_var($value, FILTER_VALIDATE_EMAIL);
    }
    
}