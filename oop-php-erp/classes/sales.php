<?php

class Sales
{
    private $table = "category";
    private $columns = ['id', 'name'];
    protected $di;
    private $database;
    private $validator;
    
    public function __construct(DependencyInjector $di)
    {
        $this->di = $di;
        $this->database = $this->di->get('database');
    }
    public function getValidator()
    {
        return $this->validator;
    }
    
    public function validateData($data)
    {
        $this->validator = $this->di->get('validator');
        $this->validator = $this->validator->check($data, [
            'customer_id'=>[
                'required'=>true
            ]
        ]);
    }
    
    public function create($data){
//        Util::dd(sizeof($_POST['product_id']));
        $this->validateData($data);
        
        if(!$this->validator->fails())
        {
            try{
                $this->database->beginTransaction();
                $data_to_be_inserted = [
                    'customer_id'=>$data['customer_id']
                ];
                $invoice_id = $this->database->insert("invoice", $data_to_be_inserted);
                
                //inserting into sales table
                for($i=0; $i< sizeof($_POST['product_id']); $i++){
                    $data_to_be_inserted = [
                        'product_id'=>$data['product_id'][$i],
                        'quantity'=>$data['quantity'][$i],
                        'discount'=>$data['discount'][$i],
                        'invoice_id'=> $invoice_id
                    ];
                    $sales_id = $this->database->insert("sales", $data_to_be_inserted);
                }
                $this->database->commit();
                return $invoice_id;
            }catch(Exception $e){
                $this->database->rollBack();
                return ADD_ERROR;
            }
        }
        return VALIDATION_ERROR;
    }
    
}