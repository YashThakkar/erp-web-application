<?php

class Customer
{
    private $table = "customers";
    private $columns = ['id', 'first_name', 'last_name', 'gst_no', 'phone_no', 'email_id', 'gender'];
    protected $di;
    private $database;
    private $validator;
    
    public function __construct(DependencyInjector $di)
    {
        $this->di = $di;
        $this->database = $this->di->get('database');
    }
    public function getValidator()
    {
        return $this->validator;
    }
    
    public function validateData($data)
    {
        $this->validator = $this->di->get('validator');
        $this->validator = $this->validator->check($data, [
            'name'=>[
                'required'=>true,
                'minlength'=>3,
                'maxlength'=>20,
                'unique'=>$this->table
            ]
        ]);
    }
    
    public function addCustomer($data)
    {
        //validate data
        $this->validateData($data);
        
        //insert data in database
        if(!$this->validator->fails())
        {
            try{
                $this->database->beginTransaction();
                $data_to_be_inserted = ['first_name'=>$data['first_name'],
                                        'last_name'=>$data['last_name'],
                                        'gst_no'=>$data['gst_no'],
                                        'phone_no'=>$data['phone_no'],
                                        'email_id'=>$data['email_id'],
                                        'gender'=>$data['gender']
                                       ];
                $customer_id = $this->database->insert($this->table, $data_to_be_inserted);
                
                //inserting into address table
                $data_to_be_inserted = ['block_no'=>$data['block_no'],
                                        'street'=>$data['street'],
                                        'city'=>$data['city'],
                                        'pincode'=>$data['pincode'],
                                        'state'=>$data['state'],
                                        'country'=>$data['country'],
                                        'town'=>$data['town']
                                       ];
                $address_id = $this->database->insert("address", $data_to_be_inserted);
                
                //Inserting Address ID and Customer ID in address_customer table
                $data_to_be_inserted = ['address_id'=> $address_id,
                                        'customer_id'=> $customer_id
                                       ];
                $address_customer_id = $this->database->insert("address_customer", $data_to_be_inserted);
                
                $this->database->commit();
                return ADD_SUCCESS;
            }catch(Exception $e){
                $this->database->rollBack();
                return ADD_ERROR;
            }
        }
        return VALIDATION_ERROR;
    }
    public function getJSONDataForDataTable($draw, $search_parameter,$order_by, $start, $length)
    {
        $query = "SELECT * FROM {$this->table} WHERE deleted = 0";
        $totalRowCountQuery = "SELECT COUNT(*) as total_count FROM {$this->table} WHERE deleted=0";
        $filteredRowCountQuery = "SELECT COUNT(*) as total_count FROM {$this->table} WHERE deleted=0";
        
        if($search_parameter != null)
        {
            $query .= " AND first_name LIKE '%{$search_parameter}%' OR last_name LIKE '%{$search_parameter}%'";
            $filteredRowCountQuery .= " AND first_name LIKE '%{$search_parameter}%' OR last_name LIKE '%{$search_parameter}%'";
        }
        if($order_by != null)
        {
            $query .= " ORDER BY {$this->columns[$order_by[0]['column']]} {$order_by[0]['dir']}";
            $filteredRowCountQuery .= " ORDER BY {$this->columns[$order_by[0]['column']]} {$order_by[0]['dir']}"; 
        }
        else
        {
            $query .= " ORDER BY {$this->columns[0]} ASC";
            $filteredRowCountQuery .= " ORDER BY {$this->columns[0]} ASC";
        }
        if($length != -1)
        {
            $query .= " LIMIT {$start}, {$length}";
        }
        
        $totalRowCountResult = $this->database->raw($totalRowCountQuery);
        $numberOfTotalRows = is_array($totalRowCountResult) ? $totalRowCountResult[0]->total_count : 0;
        
        $filteredRowCountResult = $this->database->raw($filteredRowCountQuery);
        $numberOfFilteredRows = is_array($filteredRowCountResult) ? $filteredRowCountResult[0]->total_count : 0;
        
        $fetchedData = $this->database->raw($query);
        $data = [];
        $numRows = is_array($fetchedData) ? count($fetchedData) : 0;
        for($i=0; $i<$numRows; $i++)
        {
            $subArray = [];
            $subArray[] = $start+$i+1;
            $subArray[] = $fetchedData[$i]->first_name;
            $subArray[] = $fetchedData[$i]->last_name;
            $subArray[] = $fetchedData[$i]->gst_no;
            $subArray[] = $fetchedData[$i]->phone_no;
            $subArray[] = $fetchedData[$i]->email_id;
            $subArray[] = $fetchedData[$i]->gender;
            $subArray[] = <<<BUTTONS
<button class='btn btn-outline-primary btn-sm edit' data-id='{$fetchedData[$i]->id}'><i class="fas fa-pencil-alt"></i></button>
<button class='btn btn-outline-danger btn-sm delete' data-id='{$fetchedData[$i]->id}' data-toggle="modal" data-target="#deleteModal"><i class="fas fa-trash-alt"></i></button>
BUTTONS;
            $data[] = $subArray;
        }
        
        $output = array(
            'draw'=>$draw,
            'recordsTotal'=>$numberOfTotalRows,
            'recordsFiltered'=>$numberOfFilteredRows,
            'data'=>$data
        );
        echo json_encode($output);
    }
    public function delete($id)
    {
        try{
             $this->database->beginTransaction();
             $this->database->delete($this->table, "id = {$id}");
             $this->database->commit();
             return DELETE_SUCCESS;
        }catch(Exception $e)
        {
            $this->database->rollBack();
            return DELETE_ERROR;
        }
    }
    
    public function getCustomerWithEmail($email){
        $data = array(
            "email_id" => $email
        );

        if($this->database->exists($this->table, $data)){
            $query = "SELECT id FROM {$this->table} WHERE email_id = '{$email}'";
            $fetchedData = $this->database->raw($query);
            return $fetchedData;
        }
        return false;
    }
}