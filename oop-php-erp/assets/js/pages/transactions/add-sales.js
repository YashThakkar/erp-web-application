var id = 2;

var discount = 0;
var final_rate = 0;

function deleteProduct(delete_id){
    var elements = document.getElementsByClassName("product_row");
    if(elements.length != 1){
        $("#element_"+delete_id).remove();
        subractAmount(delete_id);
    }
}

function subractAmount(element_id){
    var amt = parseInt($("#final_rate_"+(element_id).toString()).val());
    var final_total = parseInt($("#finalTotal").val());
    console.log(amt);
    final_total = final_total - amt;
    console.log(" = "+final_total);
    $("#finalTotal").val((final_total).toString());
}

function addProduct(){
    $("#products_container").append(
        `<!--BEGIN: PRODUCT CUSTOM CONTROL-->
                                <div class="row product_row" id="element_${id}">
                                   <!--BEGIN: CATEGORY SELECT-->
                                    <div class="col-md-2">
                                        <div class="form-group">
                                           <label for="">Category</label>
                                           <select id="category_${id}" class="form-control category_select">
                                               <option disabled selected>Select Category</option>
                                               <?php
                                               $categories = $di->get('database')->readData("category", ['id', 'name'],"deleted=0");
                                               ?>
                                           </select>
                                        </div>
                                    </div>
                                    <!--END: CATEGORY SELECT-->
                                    <!--BEGIN: PRODUCT SELECT-->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                           <label for="">Products</label>
                                           <select name="product_id[]" id="product_${id}" class="form-control product_select">
                                               <option disabled selected>Select Product</option>
                                           </select>
                                        </div>
                                    </div>
                                    <!--END: PRODUCT SELECT-->
                                     <!--BEGIN: SELLING PRICE-->
                                    <div class="col-md-2">
                                        <div class="form-group">
                                           <label for="">Selling Price</label>
                                           <input type="text" id="selling_price_${id}"
                                                   class="form-control selling_price" disabled>
                                        </div>
                                    </div>
                                    <!--END: SELLING PRICE-->
                                    <!--BEGIN: QUANTITY-->
                                    <div class="col-md-1">
                                        <div class="form-group">
                                           <label for="">Quantity</label>
                                           <input type="text" name="quantity[]" id="quantity_${id}"
                                                   class="form-control quantity"
                                                   value="0">
                                        </div>
                                    </div>
                                    <!--END: QUANTITY-->
                                    <!--BEGIN: DISCOUNT-->
                                    <div class="col-md-1">
                                        <div class="form-group">
                                           <label for="">Discount</label>
                                           <input type="text" name="discount[]" id="discount_${id}"
                                                   class="form-control discount"
                                                   value="0">
                                        </div>
                                    </div>
                                    <!--END: DISCOUNT-->
                                    <!--BEGIN: Final Rate PRICE-->
                                    <div class="col-md-2">
                                        <div class="form-group">
                                           <label for="">Final Rate</label>
                                           <input type="text" name="final_rate[]" id="final_rate_${id}"
                                                   class="form-control"
                                                   value="0" readonly>
                                        </div>
                                    </div>
                                    <!--END: Final Rate PRICE-->
                                    <!--BEGIN: DELETE BUTTON-->
                                    <div class="col-md-1">
                                       <button onclick="deleteProduct(${id})"
                                           type="button"
                                            class="btn btn-danger"
                                            style="margin-top: 40%">
                                            <i class="far fa-trash-alt"></i>
                                        </button>
                                    </div>
                                    <!--END: DELETE BUTTON-->
                                </div>
                                <!--END: PRODUCT CUSTOM CONTROL-->`);
    
    var baseURL = window.location.origin;
    var filePath = "/helper/routing.php";
    $.ajax({
        url: baseURL+filePath,
        method: 'POST',
        data: {
          getCategories: true  
        },
        dataType: 'json',
        success: function(categories){
            categories.forEach(function(category){
                $("#category_"+id).append(
                    `<option value='${category.id}'>${category.name}</option>`
                );    
            });
            id++;
        }
    });
}

//using event delegation
$("#products_container").on('change', '.category_select', function(){
    var element_id = $(this).attr('id').split("_")[1];
    var category_id = this.value;
    var baseURL = window.location.origin;
    var filePath = "/helper/routing.php";
    $.ajax({
        url: baseURL+filePath,
        method: 'POST',
        data: {
            getProductsByCategoryID: true,
            categoryID: category_id
        },
        dataType: 'json',
        success: function(products){
            $("#product_"+element_id).empty();
            $("#product_"+element_id).append("<option disabled selected>Select Product</option>");
            products.forEach(function(product){
                $("#product_"+element_id).append(
                    `<option value='${product.id}'>${product.name}</option>`
                );
            });
        }
    })
});
$("#products_container").on('change', '.product_select', function(){
    var element_id = $(this).attr('id').split("_")[1];
    var product_id = this.value;
    var baseURL = window.location.origin;
    var filePath = "/helper/routing.php";
    $.ajax({
        url: baseURL+filePath,
        method: 'POST',
        data: {
            getSellingPriceByProductID: true,
            productID: product_id
        },
        dataType: 'json',
        success: function(sellingRate){
            $("#selling_price_"+element_id).val(sellingRate[0].selling_rate);
        }
    })
});
$("#products_container").on('keyup', '.quantity', function(){
    var element_id = $(this).attr('id').split("_")[1];
    var product_id = this.value;
    var discount = $("#discount_"+element_id).val();
    var quantity = parseInt($("#quantity_"+element_id).val());
    
    if(!isNaN(quantity)){
        calulate_discount(discount, element_id);

        calculate_final_total(element_id);
    }
});

    
    
$("#products_container").on('keyup', '.discount', function(){
    var element_id = $(this).attr('id').split("_")[1];
    var discount = parseInt($("#discount_"+element_id).val());
    
    if(!isNaN(discount)){
        calulate_discount(discount, element_id);

        calculate_final_total(element_id);
    }
}); 


function calulate_discount( discount, element_id){
    
    var selling_price = parseInt($("#selling_price_"+element_id).val());
    var quantity = parseInt($("#quantity_"+element_id).val());
    
    final_rate = selling_price * quantity;
    if(discount == 0){
        final_rate = selling_price * quantity;
    }
    else{
       discount = ((discount/100) * (selling_price * quantity)); 
        final_rate = final_rate - discount;
    }
    
    $("#final_rate_"+element_id).val((final_rate.toString()));
     
}
function calculate_final_total(element_id){
    
    var final_total = 0;
    var row = document.getElementsByClassName("product_row");
    var number_of_products = row.length;
    for($i = 1; $i<=number_of_products; $i++){
        
        final_total += parseFloat($("#final_rate_"+($i).toString()).val());
    }
//    console.log("final total: "+final_total);
    $("#finalTotal").val((final_total).toString());
}

function getCustomerWithEmail(){
    var email = $("#customer_email").val();
    
    var baseURL = window.location.origin;
    var filePath = "/helper/routing.php";
    $.ajax({
        url: baseURL+filePath,
        method: 'POST',
        data: {
            getCustomerWithEmail: true,
            email: email
        },
        dataType: 'json',
        success: function(email){
            if(email == false){
                $("#email_verify_fail").removeClass("set-invisible");
                
                $("#email_verify_success").addClass("set-invisible");
                
                $("#add_customer_btn").removeClass("set-invisible");
            }
            else{
                $("#email_verify_success").removeClass("set-invisible");
                
                $("#email_verify_fail").addClass("set-invisible");
                
                $("#add_customer_btn").addClass("set-invisible");
                
                $("#customer_id").val(email[0].id);
            }
            
        }
    })
    
}