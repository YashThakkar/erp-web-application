<?php
require_once __DIR__ ."/../../helper/init.php";
$page_title = "QUICK ERP | Add Supplier";
$sidebarSection = 'supplier';
$sidebarSubSection = 'add';
Util::createCSRFToken();
//Util::dd(Session::getSession('csrf_token'));

$errors = "";
$old = "";
if(Session::hasSession('old'))
{
    $old = Session::getSession('old');
    Session::unsetSession('old');
}
if(Session::hasSession('errors'))
{
    $errors = unserialize(Session::getSession('errors'));
    Session::unsetSession('errors');
}

?>


<!DOCTYPE html>
<html lang="en">

<head>

<?php require_once __DIR__ . "/../includes/head-section.php"; ?>
    
</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <?php require_once __DIR__ . "/../includes/sidebar.php"; ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <?php require_once __DIR__ . "/../includes/navbar.php"; ?>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between">
            <h1 class="h3 mb-4 text-gray-800">Add Supplier</h1>
            <a href="" class="d-none d-sm-inline-nlock btn btn-sm btn-primary shadow-sm">
                <i class="fas fa-list-ul fa-sm text-white"></i>Manage Supplier
            </a>                  
          </div>
          
          <div class="container-fluid">
              <div class="row">
                  <div class="col-md-12">
                      <div class="card shadow mb-4">
                          <div class="card-header">
                              <h6 class="m-0 font-weight-bold text-primary">
                                  <i class="fa fa-plus"></i>Add Supplier
                              </h6>
                          </div>
                        <!--END OF CARD HEADER-->
                        
                        <!--CARD BODY-->
                        <div class="card-body">
                            <form action="<?= BASEURL?>helper/routing.php" method="POST">
                               <input type="hidden"
                                       name="csrf_token"
                                       value="<?= Session::getSession('csrf_token');?>">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">First Name</label>
                                            <input type="text" 
                                                   class="form-control<?= $errors!='' ? ($errors->has('first_name') ? ' error is-invalid' : '') : '';?>"
                                                    name = "first_name"
                                                    id = "first_name"
                                                    placeholder = "Enter First Name"
                                                    value="<?= $old != '' ? $old['first_name']: '';?>"
                                            >
                                            <?php
                                            if($errors!="" && $errors->has('first_name')):
                                                echo "<span class='error'> {$errors->first('first_name')}</span>";
                                            endif;
                                            ?>
                                            
                                            <label for="">Last Name</label>
                                            <input type="text" 
                                                   class="form-control<?= $errors!='' ? ($errors->has('last_name') ? ' error is-invalid' : '') : '';?>"
                                                    name = "last_name"
                                                    id = "last_name"
                                                    placeholder = "Enter Last Name"
                                                    value="<?= $old != '' ? $old['last_name']: '';?>"
                                            >
                                            <?php
                                            if($errors!="" && $errors->has('last_name')):
                                                echo "<span class='error'> {$errors->first('last_name')}</span>";
                                            endif;
                                            ?>
                                            
                                            <label for="">Gst Number</label>
                                            <input type="text" 
                                                   class="form-control<?= $errors!='' ? ($errors->has('gst_no') ? ' error is-invalid' : '') : '';?>"
                                                    name = "gst_no"
                                                    id = "gst_no"
                                                    placeholder = "Enter Gst Number"
                                                    value="<?= $old != '' ? $old['gst_no']: '';?>"
                                            >
                                            <?php
                                            if($errors!="" && $errors->has('gst_no')):
                                                echo "<span class='error'> {$errors->first('gst_no')}</span>";
                                            endif;
                                            ?>
                                            
                                            <label for="">Phone Number</label>
                                            <input type="text" 
                                                   class="form-control<?= $errors!='' ? ($errors->has('phone_no') ? ' error is-invalid' : '') : '';?>"
                                                    name = "phone_no"
                                                    id = "phone_no"
                                                    placeholder = "Enter Phone Number"
                                                    value="<?= $old != '' ? $old['phone_no']: '';?>"
                                            >
                                            <?php
                                            if($errors!="" && $errors->has('phone_no')):
                                                echo "<span class='error'> {$errors->first('phone_no')}</span>";
                                            endif;
                                            ?>
                                            
                                            <label for="">Email Id</label>
                                            <input type="text" 
                                                   class="form-control<?= $errors!='' ? ($errors->has('email_id') ? ' error is-invalid' : '') : '';?>"
                                                    name = "email_id"
                                                    id = "email_id"
                                                    placeholder = "Enter Email Id"
                                                    value="<?= $old != '' ? $old['email_id']: '';?>"
                                            >
                                            <?php
                                            if($errors!="" && $errors->has('email_id')):
                                                echo "<span class='error'> {$errors->first('email_id')}</span>";
                                            endif;
                                            ?>
                                            
                                            <label for="">Company Name</label>
                                            <input type="text" 
                                                   class="form-control<?= $errors!='' ? ($errors->has('company_name') ? ' error is-invalid' : '') : '';?>"
                                                    name = "company_name"
                                                    id = "company_name"
                                                    placeholder = "Enter Company Name"
                                                    value="<?= $old != '' ? $old['company_name']: '';?>"
                                            >
                                            <?php
                                            if($errors!="" && $errors->has('company_name')):
                                                echo "<span class='error'> {$errors->first('company_name')}</span>";
                                            endif;
                                            ?>
                                            
                                        </div>
                                    </div>
                            </div>
                            <input type="submit" class="btn btn-primary" name="add_supplier" value="submit" id="add-supplier">
                            </form>
                        </div>
                      </div>
                  </div>
              </div>
          </div>
          

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <?php require_once __DIR__ . "/../includes/footer.php"; ?>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
<?php
require_once __DIR__."/../includes/scroll-to-top.php"; 
?>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>

<?php
 require_once __DIR__ ."/../includes/page-level/core-scripts.php"  ; 
?>
 
<?php  
  require_once __DIR__ ."/../includes/page-level/index-scripts.php"  ;
?>
 
 <script src="<?BASEASSETS;?>js/plugins/jquery-validation/jquery.validate.min.js"></script>
 <script src="<?BASEASSETS;?>js/pages/supplier/add-supplier.js"></script>
  
</body>

</html>
