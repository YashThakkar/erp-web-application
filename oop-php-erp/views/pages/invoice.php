
<!--select customer_id as customer_id from invoice where id = 1 INNER JOIN (SELECT first_name, last_name, gst_no, email_id, phone_no, gender from customers where id = customer_id) as customer_details;-->

<!--select customers.first_name, customers.last_name,customers.gst_no, customers.phone_no, customers.email_id,customers.gender,GROUP_CONCAT(CONCAT(address.block_no, " ", address.street, " ", address.city, " ", address.pincode, " ", address.state, " ", address.country, " ", address.town) SEPARATOR ' , ') as address, products.name, products.specification, products.hsn_code from customers INNER JOIN invoice on customers.id = 6 INNER JOIN address_customer on address_customer.customer_id = customers.id INNER JOIN address on address.id = customers.id INNER JOIN sales on sales.invoice_id = 6 INNER JOIN products on sales.product_id = products.id GROUP By products.id-->
<?php
require_once __DIR__ ."/../../helper/init.php";
$page_title = "QUICK ERP | Invoice";

$database = $di->get('database');
$inv_id = $_GET['id'];
//Util::dd($inv_id);

//select customers.first_name, customers.last_name,customers.gst_no, customers.phone_no, customers.email_id,customers.gender,GROUP_CONCAT(CONCAT(address.block_no, " ", address.street, " ", address.city, " ", address.pincode, " ", address.state, " ", address.country, " ", address.town) SEPARATOR ' , ') as address, products.name, products.specification, products.hsn_code from customers INNER JOIN invoice on invoice.id = 6 INNER JOIN customers as customer on customer.id = invoice.customer_id INNER JOIN address_customer on address_customer.customer_id = customer.id INNER JOIN address on address.id = address_customer.address_id INNER JOIN sales on sales.invoice_id = 6 INNER JOIN products on sales.product_id = products.id GROUP By products.id


$customer_id = $database->readData("invoice", ["customer_id"], "id = {$inv_id} AND deleted = 0")[0]->customer_id;

$customer_deatils = $database->readData("customers", [], "id = {$customer_id} AND deleted = 0");
//Util::dd($customer_deatils[0]->first_name);

$address_id = $database->readData("address_customer", ['address_id'], "id = {$customer_id} AND deleted = 0")[0]->address_id;

$address_details = $database->readData("address", [], "id = {$address_id} AND deleted = 0");
//Util::dd($address_details[0]->street);

$products = $database->readData("sales", [], "invoice_id = {$inv_id} AND deleted = 0");
//foreach($products as $prod){
//    $product_id = $prod->product_id;
//    $product_selling_rate = $database->readData("products_selling_rate", ['selling_rate'], "product_id = {$product_id}")[0]->selling_rate;
//    
//}
?>


<!DOCTYPE html>
<html lang="en">

<head>

<?php require_once __DIR__ . "/../includes/head-section.php"; ?>
    
</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <?php require_once __DIR__ . "/../includes/sidebar.php"; ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <?php require_once __DIR__ . "/../includes/navbar.php"; ?>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between">
            <h1 class="h3 mb-4 text-gray-800">Invoice</h1>
                              
          </div>
          
          <div class="container-fluid">
              <div class="row">
                  <div class="col-md-12">
                      <div class="card shadow mb-4">
                          <div class="card-header">
                              <h6 class="m-0 font-weight-bold text-primary">
                                  <i class="fa fa-plus"></i>Invoice
                              </h6>
                          </div>
                        <!--END OF CARD HEADER-->
                        
                        <!--CARD BODY-->
                        <div class="card-body">
                            <form action="<?= BASEURL?>helper/routing.php" method="POST">
                               <input type="hidden"
                                       name="csrf_token"
                                       value="<?= Session::getSession('csrf_token');?>">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">First Name</label>
                                            <input type="text" 
                                                   class="form-control"
                                                    name = "first_name"
                                                    id = "first_name"
                                                    value="<?= $customer_deatils[0]->first_name; ?>"
                                                    readonly
                                            >
                                        </div>
                                    </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Last Name</label>
                                            <input type="text" 
                                                   class="form-control"
                                                    name = "last_name"
                                                    id = "last_name"
                                                    value="<?= $customer_deatils[0]->last_name; ?>"
                                                    readonly
                                            >
                                        </div>
                                    </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">GST Number</label>
                                            <input type="text" 
                                                   class="form-control"
                                                    name = "gst_no"
                                                    id = "gst_no"
                                                    value="<?= $customer_deatils[0]->gst_no; ?>"
                                                    readonly
                                            >
                                        </div>
                                    </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Phone Number</label>
                                            <input type="text" 
                                                   class="form-control"
                                                    name = "phone_no"
                                                    id = "phone_no"
                                                    value="<?= $customer_deatils[0]->phone_no; ?>"
                                                    readonly
                                            >
                                        </div>
                                    </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Email ID</label>
                                            <input type="text" 
                                                   class="form-control"
                                                    name = "email_id"
                                                    id = "email_id"
                                                    value="<?= $customer_deatils[0]->email_id; ?>"
                                                    readonly
                                            >
                                        </div>
                                    </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Gender</label>
                                            <input type="text" 
                                                   class="form-control"
                                                    name = "email_id"
                                                    id = "email_id"
                                                    value="<?= $customer_deatils[0]->gender; ?>"
                                                    readonly
                                            >
                                        </div>
                                    </div>
                                    <!-------------Address Fileds------------->
                                     <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Block Number</label>
                                            <input type="text" 
                                                   class="form-control"
                                                    name = "block_no"
                                                    id = "block_no"
                                                    value="<?= $address_details[0]->block_no; ?>"
                                            >
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Street</label>
                                            <input type="text" 
                                                   class="form-control"
                                                    name = "street"
                                                    id = "street"
                                                    value="<?= $address_details[0]->street;?>"
                                            >
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">City</label>
                                            <input type="text" 
                                                   class="form-control"
                                                    name = "city"
                                                    id = "city"
                                                    value="<?= $address_details[0]->city;?>"
                                            >
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Pincode</label>
                                            <input type="text" 
                                                   class="form-control"
                                                    name = "pincode"
                                                    id = "pincode"
                                                    placeholder = "Enter Pincode"
                                                    value="<?= $address_details[0]->pincode;?>"
                                            >
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">State</label>
                                            <input type="text" 
                                                   class="form-control"
                                                    name = "state"
                                                    id = "state"
                                                    value="<?= $address_details[0]->state;?>"
                                            >
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Country</label>
                                            <input type="text" 
                                                   class="form-control"
                                                    name = "country"
                                                    id = "country"
                                                    value="<?= $address_details[0]->country;?>"
                                            >
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Town</label>
                                            <input type="text" 
                                                   class="form-control"
                                                    name = "town"
                                                    id = "town"
                                                    value="<?= $address_details[0]->town;?>"
                                            >
                                        </div>
                                    </div>
                                     
                                    <!-----------Products Details Fields--------->
<?php
foreach($products as $prod)
{
//    Util::dd($prod);
//    echo $prod->product_id;
    $product_id = $prod->product_id;
    
    $product_selling_rate = $database->readData("products_selling_rate", ['selling_rate'], "product_id = {$product_id}")[0]->selling_rate;
    
    $products_details = $database->readData("products", [], "id = {$product_id}");

?>
                                <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Product Name</label>
                                            <input type="text" 
                                                   class="form-control"
                                                    name = "product_name"
                                                    id = "product_name"
                                                    value="<?= $products_details[0]->name;?>"
                                            >
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Product Specification</label>
                                            <input type="text" 
                                                   class="form-control"
                                                    name = "product_specification"
                                                    id = "product_specification"
                                                    value="<?= $products_details[0]->specification;?>"
                                            >
                                        </div>
                                    </div>
<?php                                        
}        
?>
                                    
                                      
                                       
                            </div>
                            <input type="submit" class="btn btn-primary" name="print_invoice" value="Print" id="print-invoice">
                            </form>
                        </div>
                      </div>
                  </div>
              </div>
          </div>
          

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <?php require_once __DIR__ . "/../includes/footer.php"; ?>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
<?php
require_once __DIR__."/../includes/scroll-to-top.php"; 
?>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>

<?php
 require_once __DIR__ ."/../includes/page-level/core-scripts.php"  ; 
?>
 
<?php  
  require_once __DIR__ ."/../includes/page-level/index-scripts.php"  ;
?>
  
</body>

</html>
