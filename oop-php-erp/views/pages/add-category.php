<?php
require_once __DIR__ ."/../../helper/init.php";
$page_title = "QUICK ERP | Add Category";
$sidebarSection = 'category';
$sidebarSubSection = 'add';
Util::createCSRFToken();
//Util::dd(Session::getSession('csrf_token'));

$errors = "";
$old = "";
if(Session::hasSession('old'))
{
    $old = Session::getSession('old');
    Session::unsetSession('old');
}
if(Session::hasSession('errors'))
{
    $errors = unserialize(Session::getSession('errors'));
    Session::unsetSession('errors');
}

?>


<!DOCTYPE html>
<html lang="en">

<head>

<?php require_once __DIR__ . "/../includes/head-section.php"; ?>
    
</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <?php require_once __DIR__ . "/../includes/sidebar.php"; ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <?php require_once __DIR__ . "/../includes/navbar.php"; ?>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between">
            <h1 class="h3 mb-4 text-gray-800">Add Category</h1>
            <a href="" class="d-none d-sm-inline-nlock btn btn-sm btn-primary shadow-sm">
                <i class="fas fa-list-ul fa-sm text-white"></i>Manage Category
            </a>                  
          </div>
          
          <div class="container-fluid">
              <div class="row">
                  <div class="col-md-12">
                      <div class="card shadow mb-4">
                          <div class="card-header">
                              <h6 class="m-0 font-weight-bold text-primary">
                                  <i class="fa fa-plus"></i>Add Category
                              </h6>
                          </div>
                        <!--END OF CARD HEADER-->
                        
                        <!--CARD BODY-->
                        <div class="card-body">
                            <form action="<?= BASEURL?>helper/routing.php" method="POST">
                               <input type="hidden"
                                       name="csrf_token"
                                       value="<?= Session::getSession('csrf_token');?>">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">Category Name</label>
                                            <input type="text" 
                                                   class="form-control<?= $errors!='' ? ($errors->has('name') ? ' error is-invalid' : '') : '';?>"
                                                    name = "name"
                                                    id = "name"
                                                    placeholder = "Enter Category Name"
                                                    value="<?= $old != '' ? $old['name']: '';?>"
                                            >
                                            <?php
                                            if($errors!="" && $errors->has('name')):
                                                echo "<span class='error'> {$errors->first('name')}</span>";
                                            endif;
                                            ?>
                                        </div>
                                    </div>
                            </div>
                            <input type="submit" class="btn btn-primary" name="add_category" value="submit" id="add-category">
                            </form>
                        </div>
                      </div>
                  </div>
              </div>
          </div>
          

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <?php require_once __DIR__ . "/../includes/footer.php"; ?>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
<?php
require_once __DIR__."/../includes/scroll-to-top.php"; 
?>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>

<?php
 require_once __DIR__ ."/../includes/page-level/core-scripts.php"  ; 
?>
 
<?php  
  require_once __DIR__ ."/../includes/page-level/index-scripts.php"  ;
?>
 
 <script src="<?BASEASSETS;?>js/plugins/jquery-validation/jquery.validate.min.js"></script>
 <script src="<?BASEASSETS;?>js/pages/category/add-category.js"></script>
  
</body>

</html>
