<script src="<?=BASEASSETS;?>js/plugins/toastr/toaster.min.js"></script>
<script src="<?=BASEASSETS;?>vendor/datatables/dataTables.min.js"></script>
<script src="<?=BASEASSETS;?>js/pages/product/manage-product.js"></script>
<script>
    toastr.options = {
      "closeButton": true,
      "debug": false,
      "newestOnTop": false,
      "progressBar": true,
      "positionClass": "toast-top-right",
      "preventDuplicates": false,
      "onclick": null,
      "showDuration": "300",
      "hideDuration": "1000",
      "timeOut": "5000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    }
    
    <?php
    if(Session::hasSession(ADD_SUCCESS)):
    ?>
            toastr.success("New product has been added successfully!", "Added");
    <?php
            Session::unsetSession(ADD_SUCCESS);
        elseif(Session::hasSession(ADD_ERROR)):
    ?>
            toastr.error("Adding a product failed!", "Failed!");
    <?php
            Session::unsetSession(ADD_ERROR);
        elseif(Session::hasSession(UPDATE_ERROR)):
    ?>
            toastr.error("Failed to update the product!", "Failed");
    <?php
            Session::unsetSession(UPDATE_ERROR);
        elseif(Session::hasSession(UPDATE_SUCCESS)):
    ?>
            toastr.success("product has been updated successfully!", "Updated!");
    <?php
            Session::unsetSession(UPDATE_SUCCESS);
        elseif(Session::hasSession(DELETE_ERROR)):
    ?>
        toastr.error("There was some problem deleting product", "Failed");
    <?php
            Session::unsetSession(DELETE_ERROR);
        elseif(Session::hasSession(DELETE_SUCCESS)):
    ?>
        toastr.success("product has been deleted successfully!", "Deleted!");
    <?php
            Session::unsetSession(DELETE_SUCCESS);
        elseif(Session::hasSession('csrf')):
    ?>
            toastr.error("Unauthorized Access, Token Mismatch", "Token Error!");
    <?php
            Session::unsetSession('csrf');
        endif;
    ?>
</script>